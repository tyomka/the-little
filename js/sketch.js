/*
| DRAWING
*/

const neck_height = 10

let canvas = null

// MAIN VARIABLES WITH DEFAULT VALUES
let hat_size = 45
let head_radius = 60
let smile_coef = -6

let body_l1_width = 80
let body_l1_height = 50

let body_l2_width = 50
let body_l2_height = 50

let body_l3_width = 90
let body_l3_height = 20

let body_l4_width = 90
let body_l4_height = 20

let arm_left_height = 25

let arm2_left_width = 60
let arm3_left_width = 10

let arm_right_height = 25

let arm2_right_width = 80
let arm3_right_width = 10

let leg1_width = 30
let leg1_height = 80

let leg2_width = 30
let leg2_height = 80

let leg3_width = 30
let leg3_height = 80

//Max variables
let hat_max = 60
let head_max = 80

let body_l1_max = 90
let body_l2_max = 85
let body_l3_max = 50
let body_l4_max = 50

let arm2_max = 80
let arm3_max = 70

let leg_max = 180

function setup() {
    canvas = createCanvas(resultWidth(), resultHeight_max())
    canvas.parent("sketch")
	pixelDensity(2)
    background(255)
}

document.getElementById('save').addEventListener('click', () => saveCanvas(canvas, 'the_little', 'jpg'))

let myHeight = 600;

function draw() {
    background(255);

    rect(5, 5, resultWidth() - 10, resultHeight_max() - 10)

	// INITIALIZATION
	hat_size = parseFloat(document.getElementById("hat").value)
	head_radius = parseFloat(document.getElementById("head").value)
	smile_coef = -parseFloat(document.getElementById("smile").value)
	
	arm2_left_width = parseFloat(document.getElementById("arm2_left_width").value)
	arm3_left_width = parseFloat(document.getElementById("arm3_left_width").value)
	arm_left_height = parseFloat(document.getElementById("arm_left_height").value)

	arm2_right_width = parseFloat(document.getElementById("arm2_right_width").value)
	arm3_right_width = parseFloat(document.getElementById("arm3_right_width").value)
	arm_right_height = parseFloat(document.getElementById("arm_right_height").value)

	body_l1_width = parseFloat(document.getElementById("body_l1_width").value)
	body_l1_height = parseFloat(document.getElementById("body_l1_height").value)

	body_l2_width = parseFloat(document.getElementById("body_l2_width").value)
	body_l2_height = parseFloat(document.getElementById("body_l2_height").value)

	body_l3_width = parseFloat(document.getElementById("body_l3_width").value)
	body_l3_height = parseFloat(document.getElementById("body_l3_height").value)

	body_l4_width = parseFloat(document.getElementById("body_l4_width").value)
	body_l4_height = parseFloat(document.getElementById("body_l4_height").value)

	leg1_width = parseFloat(document.getElementById("leg1_width").value)
	leg1_height = parseFloat(document.getElementById("leg1_height").value)

	leg2_width = parseFloat(document.getElementById("leg2_width").value)
	leg2_height = parseFloat(document.getElementById("leg2_height").value)

	leg3_width = parseFloat(document.getElementById("leg3_width").value)
	leg3_height = parseFloat(document.getElementById("leg3_height").value)

    let indent = (resultHeight_max() - resultHeight()) / 2

	if (hat_size >= 30) {
    	translate(width / 2, indent + 20)
	    //HAT
    fill(document.getElementById("color_hat").value)
        rect(-hat_size / 2, 0, hat_size, hat_size / 2)
        translate(0, hat_size / 2)

        //HAT'S PENT
        rect(-hat_size * 0.9, 0, hat_size * 1.8, hat_size / 10)
        translate(0, hat_size / 10 + head_radius / 2)
    }
    else {
    	translate(width / 2, indent + head_radius / 1.5)
    }

	// HEAD

    fill(document.getElementById("color_face").value)
	ellipse(0, 0, head_radius)
	ellipse(-head_radius / 5, -head_radius / 5, 8)
	ellipse(head_radius / 5, -head_radius / 5, 8)
	//nose
	line(0, 5, -5, 0)
	line(0, 5, 5, 0)

	translate(0, head_radius * 0.25)

	//smile

	line(-head_radius / 8, 0, head_radius / 8, 0)
	line(-head_radius / 8, 0, -head_radius / 4 , smile_coef)
	line(head_radius / 8, 0, head_radius / 4 , smile_coef)
	
	translate(0, head_radius * 0.25)
	
	// NECK
	rect(-10, 0, 20, neck_height)
	
	
	translate(0, neck_height)
	
	// BODY LEVEL 1
	fill(document.getElementById("color_body_l1").value)
	rect(-body_l1_width / 2, 0, body_l1_width, body_l1_height)
	
	// HANDS
	//Hands_level1
	fill(document.getElementById("color_lefthand").value)
	rect(-body_l1_width / 2, 0, -10, arm_left_height)
	fill(document.getElementById("color_righthand").value)
	rect(body_l1_width / 2, 0, 10, arm_right_height)


	//Hands_level2

	point_arm2_left = body_l1_width * 0.5
    point_arm2_right = body_l1_width * 0.5


    if (arm2_right_width == 0) {
        point_arm2_right = 0
    } else {
    fill(document.getElementById("color_righthand").value)
    beginShape();
    vertex (body_l1_width / 2 + 10, 0)
    vertex(body_l1_width / 2 + 10 + arm2_right_width, body_l1_width * 0.5)
    vertex(body_l1_width / 2 + 10 + arm2_right_width, body_l1_width * 0.5 + arm_right_height)
    vertex(body_l1_width / 2 + 10, arm_right_height)
    vertex(body_l1_width / 2 + 10, 0)
	endShape();
    }

    if (arm2_left_width == 0) {
        point_arm2_left = 0
    } else {
    fill(document.getElementById("color_lefthand").value)
    beginShape();
    vertex (-body_l1_width / 2 - 10, 0)
    vertex(-body_l1_width / 2 - 10 - arm2_left_width, body_l1_width * 0.5)
    vertex(-body_l1_width / 2 - 10 - arm2_left_width, body_l1_width * 0.5 + arm_left_height)
    vertex(-body_l1_width / 2 - 10, arm_left_height)
    vertex(-body_l1_width / 2 - 10, 0)
	endShape();
    }

		//Hands_level3
	fill(document.getElementById("color_righthand").value)
	rect(body_l1_width / 2 + 10 + arm2_right_width, point_arm2_right, arm3_right_width, arm_right_height)
	fill(document.getElementById("color_lefthand").value)
	rect(-body_l1_width / 2 - 10 - arm2_left_width, point_arm2_left , -arm3_left_width, arm_left_height)

	    //Hands_level4
	fill(document.getElementById("color_righthand").value)
	rect(body_l1_width / 2 + 10 + arm2_right_width + arm3_right_width, point_arm2_right, 15, arm_right_height)
	fill(document.getElementById("color_lefthand").value)
	rect(-body_l1_width / 2 - 10 - arm2_left_width - arm3_left_width, point_arm2_left, -15, arm_left_height)

	translate(0, body_l1_height)
	
	// BODY LEVEL 2
	fill(document.getElementById("color_body_l2").value)
	rect(-body_l2_width / 2, 0, body_l2_width, body_l2_height)

	translate(0, body_l2_height)	

	// BODY LEVEL 3
	fill(document.getElementById("color_body_l3").value)
	rect(-body_l3_width / 2, 0, body_l3_width, body_l3_height)

	translate(0, body_l3_height)	

	// BODY LEVEL 4
	fill(document.getElementById("color_body_l4").value)
	rect(-body_l4_width / 2, 0, body_l4_width, body_l4_height)

	translate(0, body_l4_height)

	//LEGS
	if (leg1_width >= 10){
	fill(document.getElementById("color_body_leg1").value)
    	rect(-body_l4_width / 2.2, 0, -leg1_width, leg1_height)
    }
    if (leg2_width >= 10){
	fill(document.getElementById("color_body_leg2").value)
	    rect(-leg2_width / 2, 0, leg2_width, leg2_height)
    }
    if (leg3_width >= 10){
	fill(document.getElementById("color_body_leg3").value)
	    rect(body_l4_width / 2.2, 0, leg3_width, leg3_height)
	}
		fill('#FFFFFF')
}

// TOTAL WIDTH COUNT METHOD
const resultWidth = () => {
//	return 100 + max([hat_size, head_radius * 2, body_l1_width, body_l2_width, body_l3_width, body_l4_width]) + arm3_left_width + arm2_left_width + arm3_right_width + arm2_right_width
	return 100 + head_max * 2 + (arm3_max + arm2_max) * 2

}

// TOTAL HEIGHT COUNT METHOD
const resultHeight = () => {
	return 10 + hat_size + head_radius + body_l1_height + body_l2_height + body_l3_height + body_l4_height + max([leg1_height, leg2_height, leg3_height]) + hat_size * 0.10 + neck_height
}

const resultHeight_max = () => {
    return 10 + hat_max + head_max + body_l1_max + body_l2_max + body_l3_max + body_l4_max + leg_max + (hat_max * 0.10) + neck_height
}
